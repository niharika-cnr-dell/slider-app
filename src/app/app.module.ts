import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';


import { AppComponent } from './app.component';
import { InputToDbComponent } from './input-to-db/input-to-db.component';
import { StickySliderComponent } from './slider/sticky-slider.component';

import { SliderItemService } from './silder-item.service';
// import { AnnouncementPagesComponent } from './slider/announcement-pages/announcement-pages.component';


const routes = [
  { path: '', component: InputToDbComponent },
];

@NgModule({
  declarations: [
    AppComponent,
    InputToDbComponent,
    StickySliderComponent,
    // AnnouncementPagesComponent,
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    // AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    // ReactiveFormsModule,
    FormsModule,
    // tslint:disable-next-line: deprecation
        HttpModule,
  ],
  providers: [SliderItemService],
  bootstrap: [AppComponent]
})

export class AppModule { }


















// import { ReactiveFormsModule } from '@angular/forms';
// import { AppRoutingModule } from './app-routing.module';
// import { CommonService } from './common/common.service';
// import { AnnouncementPagesComponent } from './slider/announcement-pages/announcement-pages.component';
