import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class SliderItemService {
  collectionName = 'SliderData';
  uri = 'http://localhost:4000/' + this.collectionName;

  constructor(private http: HttpClient) { }

  addToDB(newSliderData: {}) {
    // console.log(newSliderData);
    this.http.post(`${this.uri}/add`, newSliderData)
        .subscribe(res => console.log('Record Added:\n', res));
  }

  getFromDB() {
    return this.http.get(`${this.uri}/get`);
  }
}
