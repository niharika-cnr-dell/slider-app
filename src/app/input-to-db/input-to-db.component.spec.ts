import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InputToDbComponent } from './input-to-db.component';

describe('InputToDbComponent', () => {
  let component: InputToDbComponent;
  let fixture: ComponentFixture<InputToDbComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InputToDbComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InputToDbComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
