import { Component, OnInit, ɵConsole } from '@angular/core';
import { item, SliderItem } from '../../../models/SliderItem';
import { SliderItemService } from '../silder-item.service';

@Component({
  selector: 'app-input-to-db',
  templateUrl: './input-to-db.component.html',
  styleUrls: ['./input-to-db.component.css', './inherited-styles.css']
})


export class InputToDbComponent implements OnInit {

  // DBvvariables
  newSliderData: item = {};
  tableData: SliderItem[];
  isColliding: boolean;
  checkedRegions = [];

  constructor(private sliderItemService: SliderItemService) { }

  // UI Variables: Labels
  tooltipText = '';
  current_user: string;
  init_char_count: number;
  max_char_count: number;
  lbl_char_count: string;

  form_header: string;
  welcome_msg: string;
  lbl_msg_type: string;
  msg_type_placeholder: string;
  lbl_msg_body: string;
  disp_header: string;
  lbl_from_dt: string;
  lbl_to_dt: string;
  lbl_region: string;
  submit_btn_txt: string;

  inp_regions = [
    {name: 'APJ', value: 'APJ'},
    {name: 'AMER', value: 'AMER'},
    {name: 'EURO', value: 'EURO'},
  ];

  init_labels(): void {
    this.current_user = 'Jane Doe';
    this.init_char_count = 0;
    this.max_char_count = 350;

    this.form_header = 'Admin Console';
    this.welcome_msg = 'Hello, ' + this.current_user + '!';
    this.lbl_msg_type = 'Message Type:';
    this.msg_type_placeholder = 'E.g. Announcement, Deals, Information, etc.';
    this.lbl_msg_body = 'Message Body:';
    this.lbl_char_count =
      this.init_char_count + '/' + this.max_char_count + ' characters';
    this.disp_header = 'Display Information';
    this.lbl_from_dt = 'From:';
    this.lbl_to_dt = 'To:';
    this.lbl_region = 'Region(s):';
    this.submit_btn_txt = 'Submit';
  }

  ngOnInit() {
    this.init_labels();
    this.getTableData(null);
  }

  isFormEmpty(): boolean {
    // console.log(Object.keys(this.newSliderData).length);
    if (Object.keys(this.newSliderData).length < 5) {
      return true;
    }
    return false;
  }

  addSliderItem(): void {
    if (!this.isFormEmpty()) {
      const minMins = 30;
      const fromTime = new Date(this.newSliderData.fromTime).getTime();
      const toTime = new Date(this.newSliderData.toTime).getTime();
      const now = new Date().getTime();

      // fromTime and toTime validity checks
      if (fromTime > toTime || fromTime < now || toTime < now) {
        alert('Enter valid Date and Time values.');
      } else if (toTime - fromTime < minMins * 60000) {
        alert('Minimum display time should be ' + minMins + ' minutes.');
      } else {
        // time collision checks
        this.hasCollisions(fromTime, toTime, () =>  {
          if (this.isColliding) {
            this.newSliderData.fromTime = '';
            this.newSliderData.toTime = '';
            alert('TIME COLLISION: CANNOT ADD TO DB');
          } else {
            // console.log(this.newSliderData);
            this.sliderItemService.addToDB(this.newSliderData);
            window.location.reload();
          }
        });
      }
    } else  {
      alert('Fill out all the fields in the form.');
    }
  }

  hasCollisions(fromTime, toTime, decideToAdd): void {
    this.getTableData(() => {
      this.isColliding = false;
      // console.log('hasCollisions() :- Typeof tableData:', typeof this.tableData);
      if (typeof this.tableData !== 'undefined') {
        this.tableData.forEach(td => {
          const ft = new Date(td.fromTime).getTime();
          const tt = new Date(td.toTime).getTime();
          // console.log('TIMES:');
          // console.log(ft, tt);
          // console.log(fromTime, toTime);
          if (
            (fromTime >= ft && toTime <= tt) ||
            (fromTime < ft && toTime > ft) ||
            (fromTime < tt && toTime > tt)
          ) {
            // console.log('TIME COLLISION!! WITH', td.msgType);
            // console.log((fromTime >= ft && toTime <= tt), (fromTime < ft && toTime > ft), (fromTime < tt && toTime > tt));
            this.isColliding = true;
          }
        });
      }
      // console.log('isColliding:', this.isColliding);

      decideToAdd();
    });
  }

  getTableData(checkCollisions: { (): void; (): void; }) {
    this.sliderItemService.getFromDB().subscribe((data: SliderItem[]) => {
      this.tableData = data;
      // console.log('getTableData():', typeof this.tableData);
      // console.log(this.tableData);
      if (checkCollisions != null) {
        checkCollisions();
      }
    });
  }

  checkEmpty(event: any)  {
    if (event.target.value.length === 0) {
      event.target.style.border = '1px dashed red';
      this.tooltipText = 'Please fill out this field.';
    } else  {
      event.target.style.border = '1px solid lightgray';
      this.tooltipText = '';
    }
  }

  // event listener displaying number of characters allowed in textarea
  onKeyUp(event: any) {
    this.lbl_char_count =
      event.target.value.length + '/' + this.max_char_count + ' characters';
  }

  updateCheckedRegions(region, event)  {
    if (event.target.checked) {
      this.checkedRegions.push(region.value);
    } else  {
      const index = this.checkedRegions.indexOf(region.value);
      this.checkedRegions.splice(index, 1);
    }
    if (this.checkedRegions.length === 0) {
      delete this.newSliderData.regions;
    } else  {
      this.newSliderData.regions = this.checkedRegions;
    }
    // console.log( 'Regions:', this.checkedRegions.toString());
  }

  clearForm(): void {
    this.newSliderData = {};
  }

  getWindowsUser(): void  {
    // var objUserInfo = new ActiveXObject("WScript.network");
  }
}
