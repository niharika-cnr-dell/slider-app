import { Component, HostListener, OnInit } from '@angular/core';
import {trigger, state, style, animate, transition, keyframes} from '@angular/animations';
import { SliderItem } from '../../../models/SliderItem';
import { SliderItemService } from '../silder-item.service';

@Component({
// tslint:disable-next-line: component-selector
  selector: 'sticky-slider',
  templateUrl: './sticky-slider.component.html',
  styleUrls: ['./sticky-slider.component.css'],

  animations: [
    trigger('openCloseRight', [
      state(
        'openRight',
        style({
          bottom: '',
          right: '0px'
        })
      ),

      state(
        'closeRight',
        style({
          bottom: '',
          right: '-260px'
        })
      ),

      transition('openRight => closeRight',
        [animate('0.4s ease-in-out'
        // keyframes([
        //   style({transform: 'translateX(0) translateX(0px)'}),
        //   style({transform: 'translateX(20%) translateX(0px)'}),
        //   style({transform: 'translateX(50%) translateX(0px)'}),
        //   style({transform: 'translateX(100%) translateX(0px)'}),
        //   style({transform: 'translateX(40%) translateX(0px)'}),
        //   style({transform: 'translateX(60%) translateX(0px)'}),
        // ])
      )]),
      transition('closeRight => openRight', [animate('0.4s ease-in-out')])
    ]),

    trigger('openCloseBottom', [
      state(
        'openBottom',
        style({
          bottom: '0px',
          right: ''
        })
      ),

      state(
        'closeBottom',
        style({
          bottom: '-275px',
          right: ''
        })
      ),

      transition('openBottom => closeBottom', [animate('0.4s ease-in-out')]),
      transition('closeBottom => openBottom', [animate('0.4s ease-in-out')])
    ])
  ]
})


export class StickySliderComponent implements OnInit {
  windowWidth: number;
  title = 'slider-app';
  drawerMsg = 'Hear Us Out!';
  currentUser = 'John Doe';
  msgType = '';
  announcementType = 'Announcement';
  msgBody = '';
  announcementContent =
    // tslint:disable-next-line: max-line-length
    'Check out this section to know about deals and discounts, and to know about maintainence information of the OSC platform.';

  // ------------------------------------------------------------------------------------------------------------------- //

  // GLOBAL VARIABLES
  isRightOpen: boolean;
  isBottomOpen: boolean;
  displayVal: string;
  rightVal: string; bottomVal: string;
  chatDisplayVal: string;

  // DB-related variables
  tableData: SliderItem[];

  constructor(private sliderItemService: SliderItemService) { }

  togglePanel() {
    if (window.innerWidth < 991) {
      this.isBottomOpen = !this.isBottomOpen;
    } else {
      this.isRightOpen = !this.isRightOpen;
    }
    // console.log('isBottomOpen: ' + this.isBottomOpen);
    // console.log('isRightOpen: ' + this.isRightOpen);
  }

  ngOnInit() {
    this.getTableData(
      // callback 1
      () => {
      this.initSliderContent(() => {
        this.initSliderUI();
      });
    },
      // // callback 2 [undesired]
      // () => {
      //   this.initSliderUI();
      // }
    );
  }

  getTableData(
    // callback 1
    initContent: { (): void; (): void; },
    // callback 2 [undesired]
    // initUI
  ) {
    this.sliderItemService.getFromDB().subscribe((data: SliderItem[]) => {
      this.tableData = data;

      if (Object.keys(this.tableData).length > 0) {
        initContent();
      }
      // initialize default content if db is empty [UNDESIRED]
      // else  {
      //   initUI();
      // }
    });
  }

  initSliderContent(initUI: { (): void; (): void; }): void {
    if (Object.keys(this.tableData).length > 0) {
      const now = new Date().getTime();
      this.tableData.forEach(td => {
        const fromTime = new Date(td.fromTime).getTime();
        const toTime = new Date(td.toTime).getTime();
        // console.log('TIMES: ', now, fromTime, toTime);
        if (now >= fromTime && now <= toTime)  {
          this.msgType = td.msgType;
          this.msgBody = td.msgBody;
          initUI();
        }
      });
    }
  }

  initSliderUI(): void  {
    this.windowWidth = window.innerWidth;
    if (window.innerWidth < 991) {
      if (Object.keys(this.tableData).length > 0) {
        this.isBottomOpen = false;
        this.isRightOpen = null;
        this.displayVal = 'none';
        this.chatDisplayVal = 'display: block';

        this.rightVal = null;
        this.bottomVal = '-275px';
      } else  {

      }
    } else {
      this.isBottomOpen = null;
      this.isRightOpen = false;
      this.displayVal = 'block';
      this.chatDisplayVal = 'display: none';

      this.rightVal = '-260px';
      this.bottomVal = null;
    }
    if (this.msgType !== '') {
      this.announcementType     = this.msgType;
    }
    if (this.msgBody !== '') {
      this.announcementContent  = this.msgBody;
    }
    // console.log('Checkout the slider!!');
  }

  // tslint:disable-next-line: member-ordering
  @HostListener('window:resize', ['$event'])
  onResize(event: { target: { innerWidth: number } }) {
    if (this.msgType !== '' && this.msgBody !== '') {
      this.ngOnInit();
    }
  }
}
