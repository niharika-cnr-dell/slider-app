import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StickySliderComponent } from './sticky-slider.component';

describe('StickySliderComponent', () => {
  let component: StickySliderComponent;
  let fixture: ComponentFixture<StickySliderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StickySliderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StickySliderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
