import {  Component,  HostListener  } from '@angular/core';
import {
  trigger,
  state,
  style,
  animate,
  transition
  // ...
} from '@angular/animations';


@Component({
  // tslint:disable-next-line: component-selector
  selector: 'app-slider',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})


export class AppComponent {
  windowWidth: number;

// tslint:disable-next-line: use-life-cycle-interface
  ngOnInit() {
    this.windowWidth = window.innerWidth;
  }

  @HostListener('window:resize', ['$event'])
  onResize() {
    this.ngOnInit();
  }
}
