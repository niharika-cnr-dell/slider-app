const express = require('express');
const app = express();
const sliderRoutes = express.Router();
const mongoose = require('mongoose');

let SliderItemSchema = require('../models/SliderItem');


sliderRoutes.route('/add').post(function (req, res) {
  let sliderItem = new SliderItemSchema(req.body);
  sliderItem.save()
    .then(sliderItem => {
      res.status(200).json({'sliderItem': 'sliderItem in added successfully'});
    })
    .catch(err => {
    res.status(400).send("unable to save to database");
    });
});

sliderRoutes.route('/get').get(function (req, res)  {
  SliderItemSchema.find(function (err, result)  {
    if (err)  {
      console.log(err);
    } else  {
      res.json(result);
    }
  });
});

module.exports = sliderRoutes;
