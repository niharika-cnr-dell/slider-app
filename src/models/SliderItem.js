// SliderItem.js

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const item = {
  msgType: String,
  msgBody: String,
  fromTime: Date,
  toTime: Date,
}

const SliderItem = new Schema(item, {collection: 'data'})

module.exports = mongoose.model('SliderItem', SliderItem);
