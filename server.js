// server.js

var express = require("express"),
  bodyParser = require("body-parser"),
  cors = require("cors"),
  mongoose = require("mongoose"),
  config = require("./DB");

const collection = '/data';

const sliderRoute = require("./routes/slider.route")
mongoose.Promise = global.Promise;
console.log(config.DB_gitlab);
mongoose.connect(config.DB_gitlab, {useNewUrlParser: true}).then(
  () => {console.log("DB Connected!")},
  err => {
    console.log("Cannot connect to the DB");
    console.log(err);
  }
);

const app = express();

app.use(bodyParser.json()),
app.use(bodyParser.urlencoded({
  extended: true
}));

app.use(cors());
app.use(collection, sliderRoute);

app.listen(4000, function () {
  console.log("Example app listening on port 4000!!");
});
